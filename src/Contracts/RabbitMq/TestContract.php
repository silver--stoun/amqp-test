<?php

declare(strict_types=1);

namespace App\Contracts\RabbitMq;

final class TestContract
{
    public function __construct(
        private string $message,
    ) {
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
