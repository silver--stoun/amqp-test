<?php

declare(strict_types=1);

namespace App\Service\Consumer;

use App\Contracts\RabbitMq\TestContract;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class TestRabbitHandler
{
    public function __invoke(TestContract $message)
    {
        print_r($message->getMessage());
    }
}
