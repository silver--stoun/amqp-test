<?php

declare(strict_types=1);

namespace App\Controller;

use App\Contracts\RabbitMq\TestContract;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

final class TestController extends AbstractController
{
    #[Route(path: '/', methods: Request::METHOD_GET,)]
    public function __invoke(MessageBusInterface $bus): JsonResponse
    {
        $message = new TestContract('it\' alive');
        $bus->dispatch($message);

        return $this->json(['All fine']);
    }
}
